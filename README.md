# README #

This Repo aims to implement a tool, that gives the possibility to automate computer inputs (mouse / keyboard).

### Features ###

* record inputs 
* create automations without recording (manually specify actions)
* execute command-line options (predefined actions)
* store automations as files
* organize automations with namings and folders
* provide predefined automations
* possibility to automate mouse actions by relative positioning