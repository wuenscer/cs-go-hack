package view;

import java.awt.Color;
import java.awt.Cursor;
import java.awt.Graphics;
import java.awt.Image;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.SwingConstants;

public class StylishButton extends JButton {

	private static final long serialVersionUID = 1L;

	private Image img = null;
	
	public StylishButton(Image image, String label){
		super(label);
		this.img = image;
	}

	public StylishButton(String label){
		super(label);
		try {
			this.img = ImageIO.read(System.class.getResource("/res/bullet.jpg"));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static void applyStyleTo(AbstractButton button){
		button.setFocusPainted(false);
		button.setBorder(BorderFactory.createLineBorder(Color.BLACK, 1, true));
		button.setFont(button.getFont().deriveFont(24f));
		button.setHorizontalAlignment(SwingConstants.LEFT);
		button.setCursor(new Cursor(Cursor.HAND_CURSOR));
		button.setBackground(Color.WHITE);
	}
	
	public void paintComponent(Graphics page) {
		super.paintComponent(page);

		if (img != null) {
			/*
			int h = img.getHeight(null);
			int w = img.getWidth(null);

			// Scale Horizontally:
			if (w > this.getWidth()) {
				img = img
						.getScaledInstance(getWidth(), -1, Image.SCALE_DEFAULT);
				h = img.getHeight(null);
			}

			// Scale Vertically:
			if (h > this.getHeight()) {
				img = img.getScaledInstance(-1, getHeight(),
						Image.SCALE_DEFAULT);
			}
			*/

			img = img.getScaledInstance(getWidth(), getHeight(),
					Image.SCALE_DEFAULT);
			
			// Center Images
			int x = 0; //(getWidth() - img.getWidth(null)) / 2;
			int y = 0; //(getHeight() - img.getHeight(null)) / 2;
			
			
			// Draw it
			page.drawImage(img, x, y, null);
		}
	}
	
}
