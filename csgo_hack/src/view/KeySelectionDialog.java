package view;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.WindowAdapter;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFormattedTextField;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.border.LineBorder;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.PlainDocument;

public class KeySelectionDialog extends JDialog {

	public static int CANCEL_OPTION = 0;
	public static int OK_OPTION = 1;
	private int dialogOption = CANCEL_OPTION;
	
	private int activationKeyCode = KeyEvent.VK_UNDEFINED;
	private int deactivationKeyCode = KeyEvent.VK_UNDEFINED;
	
	private static final long serialVersionUID = 1L;
	
	final JFormattedTextField inputDeactivationKey = new JFormattedTextField();
	final JFormattedTextField inputActivationKey = new JFormattedTextField();
	
	private ActionListener cancelAction = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			deactivationKeyCode = KeyEvent.VK_UNDEFINED;
			activationKeyCode = KeyEvent.VK_UNDEFINED;
			dialogOption = CANCEL_OPTION;
			setVisible(false);
		}
	};
	
	private ActionListener okayAction = new ActionListener() {
		@Override
		public void actionPerformed(ActionEvent arg0) {
			ArrayList<String> errors = validateInputs();
			if(errors.size() == 0){
				deactivationKeyCode = KeyEvent.getExtendedKeyCodeForChar(inputDeactivationKey.getText().charAt(0));
				activationKeyCode = KeyEvent.getExtendedKeyCodeForChar(inputActivationKey.getText().charAt(0));
				dialogOption = OK_OPTION;
				setVisible(false);
			}else{
				String errorMsg = "";
				for(String error : errors){
					errorMsg += "- "+error+"\n";
				}
				JOptionPane.showMessageDialog(KeySelectionDialog.this, errorMsg);
			}
		}
	};
	
	private MouseAdapter keyInputClickListener = new MouseAdapter() {
		public void mouseClicked(java.awt.event.MouseEvent arg0) {
			Object src = arg0.getSource();
			if(src instanceof JFormattedTextField){
				((JFormattedTextField)src).setText("");
			}
		};
	};
	
	private WindowAdapter windowListener = new WindowAdapter() {
		public void windowClosed(java.awt.event.WindowEvent arg0) {
			cancelAction.actionPerformed(null);
		};
	};
	
	public KeySelectionDialog(){
		setIconImage(Toolkit.getDefaultToolkit().getImage(KeySelectionDialog.class.getResource("/res/headshot.jpg")));
		setModal(true);
		setTitle("KeySelection");
		setUndecorated(true);
		getContentPane().setLayout(new BorderLayout(0, 0));
		setSize(200, 200);
		setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);
		addWindowListener(windowListener);
		
		JPanel content = new JPanel();
		content.setSize(200, 200);
		content.setForeground(Color.GRAY);
		content.setBackground(Color.DARK_GRAY);
		content.setBorder(new LineBorder(Color.BLACK, 3, true));
		content.setLayout(null);
		
		JLabel lblActivationCaption = new JLabel("Activation Key:");
		lblActivationCaption.setForeground(Color.LIGHT_GRAY);
		lblActivationCaption.setBounds(10, 11, 180, 14);
		content.add(lblActivationCaption);
		
		inputActivationKey.setDocument(new PlainDocument(){
			private static final long serialVersionUID = 1L;
			@Override
		    public void insertString(int offs, String str, AttributeSet a)
		            throws BadLocationException {
		        if(getLength() + str.length() <= 1)
		            super.insertString(offs, str, a);
		    }
		});
		inputActivationKey.setText("-");
		inputActivationKey.setForeground(Color.WHITE);
		inputActivationKey.setFont(inputActivationKey.getFont().deriveFont(inputActivationKey.getFont().getStyle() | Font.BOLD, 24f));
		inputActivationKey.setColumns(1);
		inputActivationKey.setBackground(Color.DARK_GRAY);
		inputActivationKey.setBounds(10, 29, 39, 29);
		inputActivationKey.addMouseListener(keyInputClickListener);
		
		content.add(inputActivationKey);
		
		JLabel lblDeactivationCaption = new JLabel("Deactivation Key:");
		lblDeactivationCaption.setForeground(Color.LIGHT_GRAY);
		lblDeactivationCaption.setBounds(10, 85, 180, 14);
		content.add(lblDeactivationCaption);
		
		inputDeactivationKey.setDocument(new PlainDocument(){
			private static final long serialVersionUID = 1L;
			@Override
		    public void insertString(int offs, String str, AttributeSet a)
		            throws BadLocationException {
		        if(getLength() + str.length() <= 1)
		            super.insertString(offs, str, a);
		    }
		});
		inputDeactivationKey.setText("-");
		inputDeactivationKey.setForeground(Color.WHITE);
		inputDeactivationKey.setFont(inputDeactivationKey.getFont().deriveFont(inputDeactivationKey.getFont().getStyle() | Font.BOLD, 24f));
		inputDeactivationKey.setColumns(1);
		inputDeactivationKey.setBackground(Color.DARK_GRAY);
		inputDeactivationKey.setBounds(10, 110, 39, 29);
		inputDeactivationKey.addMouseListener(keyInputClickListener);
		content.add(inputDeactivationKey);
		
		JButton btnCancel = new JButton("Cancel");
		btnCancel.setBounds(10, 166, 89, 23);
		content.add(btnCancel);
		
		JButton btnOkay = new JButton("Okay");
		btnOkay.setBounds(101, 166, 89, 23);
		content.add(btnOkay);
		
		btnCancel.addActionListener(cancelAction);
		btnOkay.addActionListener(okayAction);
		
		setContentPane(content);
		
		setLocationRelativeTo(null);
	}

	public int getActivationKeyCode() {
		return activationKeyCode;
	}
	
	public int getDeactivationKeyCode() {
		return deactivationKeyCode;
	}
	
	private ArrayList<String> validateInputs(){
		ArrayList<String> errors = new ArrayList<String>();
		if(inputActivationKey.getText().length()==0){
			errors.add("No activation key defined!");
		}
		if(inputDeactivationKey.getText().length()==0){
			errors.add("No deactivation key defined!");
		}
		return errors;
	}

	public int showDialog(){
		setVisible(true);
		return dialogOption;
	}
}
