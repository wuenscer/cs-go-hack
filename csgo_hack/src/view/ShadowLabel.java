package view;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.font.TextLayout;

import javax.swing.JLabel;

public class ShadowLabel extends JLabel {

	private static final long serialVersionUID = 1L;

	private Color shadowColor = new Color(150, 150, 150);
	private int shadowOffset = 3;
	
	public Color getShadowColor() {
		return shadowColor;
	}

	public void setShadowColor(Color shadowColor) {
		this.shadowColor = shadowColor;
	}

	public int getShadowOffset() {
		return shadowOffset;
	}

	public void setShadowOffset(int shadowOffset) {
		this.shadowOffset = shadowOffset;
	}

	public void paint(Graphics g) {
		int x = 0;
		int y = 0;

		Graphics2D g1 = (Graphics2D) g;

		TextLayout textLayout = new TextLayout(getText(), getFont(),
				g1.getFontRenderContext());
		g1.setPaint(getShadowColor());
		textLayout.draw(g1, x + getShadowOffset(), y + getShadowOffset());

		g1.setPaint(getForeground());
		textLayout.draw(g1, x, y);
	}

}
