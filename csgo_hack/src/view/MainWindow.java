package view;

import hack.HackControl;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

import javax.imageio.ImageIO;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JToggleButton;

import main.Main;

import org.jnativehook.NativeHookException;

public class MainWindow extends JFrame{

	private static final long serialVersionUID = 1L;
	
	private BackgroundPanel bgPanel = null;
	private int buttonWidth = 150;
	private int buttonHeight = 50;
	
	private KeySelectionDialog activationKeySelection = new KeySelectionDialog();
	private JLabel hackStatusLabel = new JLabel("Status: Hack is inactive");
	
	private Image penis = null;
	
	public MainWindow(){
		try {
			penis = ImageIO.read(System.class.getResource("/res/enlargement.png"));
		} catch (IOException e) {
			e.printStackTrace();
		}
		setLayout(new BorderLayout());
		try {
			setIconImage(ImageIO.read(System.class.getResource("/res/headshot.jpg")));
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		setUndecorated(true);
		setSize(800, 600);
		
		
		final JPanel buttonContainer = new JPanel(null);
		
		try {
			this.bgPanel = new BackgroundPanel(ImageIO.read(System.class.getResource("/res/bg.png")));
		} catch (IOException e) {
			e.printStackTrace();
			this.bgPanel = new BackgroundPanel(null);
		}
		bgPanel.setLayout(null);
		bgPanel.setBorder(BorderFactory.createLineBorder(Color.DARK_GRAY, 1, true));
		JToggleButton toggleStartStop = new JToggleButton("Start");
		StylishButton.applyStyleTo(toggleStartStop);
		try {
			toggleStartStop.setIcon(new ImageIcon(ImageIO.read(System.class.getResource("/res/bullet.jpg")).getScaledInstance(50, -1, Image.SCALE_DEFAULT)));
			toggleStartStop.setBackground(Color.WHITE);
		} catch (IOException e) {
			e.printStackTrace();
			toggleStartStop.setForeground(Color.LIGHT_GRAY);
			toggleStartStop.setBackground(Color.DARK_GRAY);
		}
		toggleStartStop.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				JToggleButton source = null;
				if(arg0.getSource() instanceof JToggleButton){
					source = (JToggleButton)arg0.getSource();
					if(source.isSelected()){
						//START WAS CLICKED
						System.out.println("Start was clicked!");
						int dialogOption = activationKeySelection.showDialog();
						int activationKey = activationKeySelection.getActivationKeyCode();
						int deactivationKey = activationKeySelection.getDeactivationKeyCode();
						if(dialogOption == KeySelectionDialog.OK_OPTION){
							try {
								HackControl.initActivationListener(activationKey, deactivationKey);
								source.setText("Stop");
							} catch (NativeHookException e) {
								e.printStackTrace();
								source.setSelected(false);
								JOptionPane.showMessageDialog(null, "Could not start hack:\n\n"+e);
							}
						}else{
							source.setSelected(false);
						}
						
					}else{
						//STOP WAS CLICKED
						System.out.println("Stop was clicked!");
						HackControl.unregisterActivationListener();
						source.setText("Start");
					}
				}
			}
		});
		toggleStartStop.setBounds(30, 170, buttonWidth, buttonHeight);
		
		JButton quitBtn = new JButton("Exit");
		StylishButton.applyStyleTo(quitBtn);
		try {
			quitBtn.setIcon(new ImageIcon(ImageIO.read(System.class.getResource("/res/bullet.jpg")).getScaledInstance(50, -1, Image.SCALE_DEFAULT)));
			quitBtn.setBackground(Color.WHITE);
		} catch (IOException e) {
			e.printStackTrace();
			quitBtn.setForeground(Color.LIGHT_GRAY);
			quitBtn.setBackground(Color.DARK_GRAY);
		}
		quitBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				Main.exit();
			}
		});
		quitBtn.setBounds(toggleStartStop.getX(), toggleStartStop.getY()+toggleStartStop.getHeight()+10,
				buttonWidth, buttonHeight);
		
		JCheckBox highDropCheckBox = new JCheckBox("High Drop Rate");
		StylishButton.applyStyleTo(highDropCheckBox);
		try {
			Image esportsCase1 = ImageIO.read(System.class.getResource("/res/drop_crossed.jpg")).getScaledInstance(80, -1, Image.SCALE_DEFAULT);
			Image esportsCase2 = ImageIO.read(System.class.getResource("/res/drop_ticked.jpg")).getScaledInstance(80, -1, Image.SCALE_DEFAULT);
			highDropCheckBox.setIcon(new ImageIcon(esportsCase1));
			highDropCheckBox.setSelectedIcon(new ImageIcon(esportsCase2));
		} catch (IOException e) {
			e.printStackTrace();
		}
		highDropCheckBox.setBounds(20+toggleStartStop.getX()+toggleStartStop.getWidth(), toggleStartStop.getY(), 
				highDropCheckBox.getPreferredSize().width, highDropCheckBox.getPreferredSize().height);
		
		
		final JLabel penisLabel = new JLabel();
		penisLabel.setBounds(getWidth()-250, (getHeight()/2)+5, 100, 50);
		
		final JCheckBox penisEnlargement = new JCheckBox("Penis Enlargement");
		StylishButton.applyStyleTo(penisEnlargement);
		try {
			Image enlargement1 = ImageIO.read(System.class.getResource("/res/enlargement_crossed.jpg")).getScaledInstance(80, -1, Image.SCALE_DEFAULT);
			Image enlargement2 = ImageIO.read(System.class.getResource("/res/enlargement_ticked.jpg")).getScaledInstance(80, -1, Image.SCALE_DEFAULT);
			penisEnlargement.setIcon(new ImageIcon(enlargement1));
			penisEnlargement.setSelectedIcon(new ImageIcon(enlargement2));
		} catch (IOException e) {
			e.printStackTrace();
		}
		penisEnlargement.setBounds(highDropCheckBox.getX(), highDropCheckBox.getY()+highDropCheckBox.getHeight()+10, 
				penisEnlargement.getPreferredSize().width, penisEnlargement.getPreferredSize().height);
		penisEnlargement.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent arg0) {
				if(penisEnlargement.isSelected()){
					penisLabel.setIcon(new ImageIcon(penis));
					penisLabel.revalidate();
				}else{
					penisLabel.setIcon(null);
					penisLabel.revalidate();
				}
			}
		});
		
		
		hackStatusLabel.setFont(hackStatusLabel.getFont().deriveFont(Font.BOLD, 16f));
		hackStatusLabel.setBounds(quitBtn.getX(), quitBtn.getY()+quitBtn.getHeight()+30, getWidth(), hackStatusLabel.getPreferredSize().height);
		
		new Thread(new Runnable() {
			@Override
			public void run() {
				while(!Thread.interrupted()){
					if(HackControl.isHackActive()){
						hackStatusLabel.setText("Status: Hack is active!");
						hackStatusLabel.setForeground(Color.GREEN);
					}else if(HackControl.isActivationListenerRegistered()){
						hackStatusLabel.setText("Status: Hack is waiting for activation...");
						hackStatusLabel.setForeground(Color.ORANGE);
					}else{
						hackStatusLabel.setText("Status: Hack is inactive");
						hackStatusLabel.setForeground(Color.RED);
					}
				}
			}
		}, "Status updater").start();
		
		buttonContainer.setSize(new Dimension(getWidth(), getHeight()));
		buttonContainer.setOpaque(false);
		buttonContainer.add(toggleStartStop);
		buttonContainer.add(quitBtn);
		buttonContainer.add(hackStatusLabel);
		buttonContainer.add(highDropCheckBox);
		buttonContainer.add(penisEnlargement);
		buttonContainer.add(penisLabel);

		add(bgPanel, BorderLayout.CENTER);
		
		buttonContainer.setBounds(0, 0, buttonContainer.getWidth(), buttonContainer.getHeight());
		
		bgPanel.add(buttonContainer);
		
		
		setLocationRelativeTo(null);
	}
	
}
