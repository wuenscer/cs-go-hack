package view;

import java.awt.Graphics;
import java.awt.Image;

import javax.swing.JPanel;

public class BackgroundPanel extends JPanel {

	private static final long serialVersionUID = 1L;

	private Image img = null;

	public BackgroundPanel(Image image) {
		this.img = image;
	}

	public void paintComponent(Graphics page) {
		super.paintComponent(page);

		if (img != null) {
			/*
			int h = img.getHeight(null);
			int w = img.getWidth(null);

			// Scale Horizontally:
			if (w > this.getWidth()) {
				img = img
						.getScaledInstance(getWidth(), -1, Image.SCALE_DEFAULT);
				h = img.getHeight(null);
			}

			// Scale Vertically:
			if (h > this.getHeight()) {
				img = img.getScaledInstance(-1, getHeight(),
						Image.SCALE_DEFAULT);
			}
			*/

			img = img.getScaledInstance(getWidth(), getHeight(),
					Image.SCALE_DEFAULT);
			
			// Center Images
			int x = 0; //(getWidth() - img.getWidth(null)) / 2;
			int y = 0; //(getHeight() - img.getHeight(null)) / 2;
			
			
			// Draw it
			page.drawImage(img, x, y, null);
		}
	}

}
