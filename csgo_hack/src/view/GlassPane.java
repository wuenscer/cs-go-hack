package view;

import hack.HitThread;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Font;
import java.awt.GraphicsConfiguration;
import java.awt.GraphicsEnvironment;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingConstants;

public class GlassPane extends JFrame {
	private static final long serialVersionUID = 1L;

	public GlassPane(GraphicsConfiguration gc){
		super(gc);
		setUndecorated(true);
		setBackground(Color.WHITE);
		setOpacity(0.4f);
		setLayout(new BorderLayout());
		setBounds(gc.getBounds());
		setAlwaysOnTop(true);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		final JLabel heading = new JLabel("GlassPane");
		heading.setFont(new Font(Font.SERIF, Font.BOLD, 24));
		heading.setForeground(Color.BLACK);
		heading.setHorizontalAlignment(SwingConstants.CENTER);
		heading.setVerticalAlignment(SwingConstants.TOP);
		heading.addMouseListener(new MouseListener() {
			@Override
			public void mouseReleased(MouseEvent arg0) {}
			@Override
			public void mousePressed(MouseEvent arg0) {}
			@Override
			public void mouseExited(MouseEvent arg0) {}
			@Override
			public void mouseEntered(MouseEvent arg0) {}
			@Override
			public void mouseClicked(MouseEvent arg0) {
				new HitThread(heading, arg0.getX(), arg0.getY()).start();
			}
		});
		add(heading, BorderLayout.CENTER);
	}
	
	public GlassPane() {
		this(GraphicsEnvironment.getLocalGraphicsEnvironment().getDefaultScreenDevice().getDefaultConfiguration());
	}
}
