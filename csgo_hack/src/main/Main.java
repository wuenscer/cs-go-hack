package main;

import hack.HackControl;

import javax.swing.SwingUtilities;

import view.MainWindow;

public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				new MainWindow().setVisible(true);
			}
		});
	}

	public static void exit(){
		HackControl.stopHack();
		HackControl.unregisterActivationListener();
		System.runFinalization();
		System.exit(0);
	}
}
