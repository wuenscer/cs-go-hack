package main;

import hack.HackControl;

import java.awt.event.KeyEvent;

import javax.swing.JOptionPane;
import javax.swing.SwingUtilities;

import org.jnativehook.GlobalScreen;
import org.jnativehook.NativeHookException;
import org.jnativehook.keyboard.NativeKeyEvent;
import org.jnativehook.keyboard.NativeKeyListener;

import view.GlassPane;

public class Test {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				int result = JOptionPane.showConfirmDialog(null, "This test will display a window, on which the hack will randomly click.\n" +
						"Clicks are visualized by increasing circles.\nYou can stop the Test by pressing >ESCAPE<",
						"Test Confirmation", JOptionPane.OK_CANCEL_OPTION);
				if(result == JOptionPane.CANCEL_OPTION){
					return;
				}
				final GlassPane glassPane = new GlassPane();
				try {
					GlobalScreen.registerNativeHook();
					GlobalScreen.getInstance().addNativeKeyListener(new NativeKeyListener() {
						@Override
						public void nativeKeyTyped(NativeKeyEvent arg0) {}
						@Override
						public void nativeKeyReleased(NativeKeyEvent arg0) {
							if(arg0.getKeyCode() == KeyEvent.VK_ESCAPE){
								SwingUtilities.invokeLater(new Runnable() {
									@Override
									public void run() {
										HackControl.stopHack();
										GlobalScreen.unregisterNativeHook();
										glassPane.setVisible(false);
										glassPane.dispose();
									}
								});
							}
						}
						@Override
						public void nativeKeyPressed(NativeKeyEvent arg0) {}
					});
					glassPane.setVisible(true);
					HackControl.startHack();
				} catch (NativeHookException e) {
					e.printStackTrace();
					JOptionPane.showMessageDialog(null, "Could not initialize hack!\nException:\n"+e.getMessage()+"\n\nProgram will exit :(");
				}
			}
		});
	}

}
