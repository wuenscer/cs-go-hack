package hack;

import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.MouseInfo;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.event.InputEvent;
import java.util.HashMap;
import java.util.Random;

import javax.swing.SwingUtilities;

import view.GlassPane;

public class HackThread extends Thread{

	private static Random rand = new Random();
	
	public HackThread(){
		setName("CS:GO-Hack");
	}
	
	@Override
	public void run() {
		System.out.println("Init hacking thread...");
		HashMap<Robot, GraphicsDevice> robotDeviceMap = new HashMap<Robot, GraphicsDevice>();
		HashMap<GraphicsDevice, GlassPane> deviceDrawingHashMap = new HashMap<GraphicsDevice, GlassPane>();
		try {
			GraphicsDevice[] devices = GraphicsEnvironment.getLocalGraphicsEnvironment().getScreenDevices();
			System.out.println("- Found "+devices.length+" graphics devices");
			int debugRobotCount = 0;
			for(GraphicsDevice d : devices){
				//Rectangle rect = d.getDefaultConfiguration().getBounds();
				//GlassPane gp = new GlassPane(d.getDefaultConfiguration());
				//gp.setBounds(rect);
				//gp.setVisible(true);
				//deviceDrawingHashMap.put(d, gp);
				Robot r = new Robot(d);
				robotDeviceMap.put(r, d);
				debugRobotCount++;
				System.out.println("- Created graphics robot "+debugRobotCount);
			}
			System.out.println("Initialilization finished!");
		} catch (Exception e){
			e.printStackTrace();
			this.interrupt();
		}
		System.out.println("Let the hacking begin!");
		while(!isInterrupted()){
			for(Robot r : robotDeviceMap.keySet()){
				GraphicsDevice d = robotDeviceMap.get(r);
				//if(d.getFullScreenWindow()!=null){
				Rectangle rect = d.getDefaultConfiguration().getBounds();
				Point p = getRandomScreenPoint(rect.width, rect.height);
				p = new Point(p.x, MouseInfo.getPointerInfo().getLocation().y);
				r.mouseMove(p.x, p.y);
				r.mousePress(InputEvent.BUTTON1_MASK);
				r.mouseRelease(InputEvent.BUTTON1_MASK);
				System.out.println("Robot ("+r.toString()+") clicked at X:" + p.x + " Y:" + p.y);
				//}
			}
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				interrupt();
			}
		}
		for(final GlassPane gp : deviceDrawingHashMap.values()){
			SwingUtilities.invokeLater(new Runnable() {
				@Override
				public void run() {
					gp.setVisible(false);
					gp.dispose();
				}
			});
		}
		System.err.println("Hacking was interrupted!");
	}
	
	
	private Point getRandomScreenPoint(int maxWidth, int maxHeight){
		int min_w = 0;
		int max_w = 0;
		if(maxWidth > 0){
			max_w = maxWidth;
		}
		int min_h = 0;
		int max_h = 0;
		if(maxHeight > 0){
			max_h = maxHeight;
		}
		
		// nextInt is normally exclusive of the top value,
		// so add 1 to make it inclusive
		int randomX = rand.nextInt((max_w - min_w) + 1) + min_w;
		int randomY = rand.nextInt((max_h - min_h) + 1) + min_h;

		return new Point(randomX, randomY);
	}
}
