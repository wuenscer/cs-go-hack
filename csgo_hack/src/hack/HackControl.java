package hack;

import java.awt.event.KeyEvent;

import org.jnativehook.GlobalScreen;
import org.jnativehook.NativeHookException;

public class HackControl {

	private static ActivationListener activationListener = null;
	
	private static Thread hackingThread = new HackThread();
	
	public static boolean isHackActive() {
		if(hackingThread.isAlive()){
			return true;
		}
		return false;
	}

	public static void startHack(){
		if(!isHackActive()){
			if(hackingThread.isAlive()){
				hackingThread.interrupt();
			}
			hackingThread = new HackThread();
			hackingThread.start();
			System.out.println("Started hack!");
		}else{
			System.out.println("Start attempt - Hack already active!");
		}
	}
	
	public static void stopHack(){
		if(isHackActive()){
			hackingThread.interrupt();
			try {
				hackingThread.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			System.out.println("Stopped hack!");
		}else{
			System.out.println("Stop attempt - Hack already stopped!");
		}
	}
	
	public static void initActivationListener(int activationKeyCode, int deactivationKeyCode) throws NativeHookException{
		//check for valid keycodes
		if(activationKeyCode == KeyEvent.VK_UNDEFINED || deactivationKeyCode == KeyEvent.VK_UNDEFINED){
			throw new NativeHookException("One or more keycodes aren't valid:\nActivation("+KeyEvent.getKeyText(activationKeyCode)+"), Deactivation("+KeyEvent.getKeyText(deactivationKeyCode)+")");
		}
		if(GlobalScreen.isNativeHookRegistered()){
			GlobalScreen.getInstance().removeNativeKeyListener(activationListener);
		}else{
			GlobalScreen.registerNativeHook();
		}
		activationListener = new ActivationListener(activationKeyCode, deactivationKeyCode);
		GlobalScreen.getInstance().addNativeKeyListener(activationListener);
		System.out.println("Global KeyListener added: Activation("+KeyEvent.getKeyText(activationKeyCode)+"), Deactivation("+KeyEvent.getKeyText(deactivationKeyCode)+")");
	}

	public static boolean isActivationListenerRegistered() {
		if(activationListener != null){
			return true;
		}
		return false;
	}

	public static void unregisterActivationListener() {
		GlobalScreen.getInstance().removeNativeKeyListener(activationListener);
		activationListener = null;
		GlobalScreen.unregisterNativeHook();
		System.out.println("Unregistered native KeyListener");
	}
	
}
