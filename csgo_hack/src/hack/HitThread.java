package hack;

import java.awt.Color;
import java.awt.Component;
import java.awt.Graphics;

public class HitThread extends Thread {
	
	private int x = 0;
	private int y = 0;
	private int size = 2;
	private Component component = null;
	
	public HitThread(Component comp, int x, int y){
		this.x = x;
		this.y = y;
		this.component = comp;
	}
	
	@Override
	public void run() {
		Graphics g = component.getGraphics();
		while(!interrupted() && size <= 20){
			int timesToDraw = size/2;
			for(int i = 1; i <= timesToDraw; i++){
				int tempSize = (2*timesToDraw);
				int tempX = x - (timesToDraw);
				int tempY = y - (timesToDraw);
				g.setColor(Color.RED);
				g.drawOval(tempX, tempY, tempSize, tempSize);
			}
			size += 2;
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		g.setColor(component.getBackground());
		g.fillRect(x-(size/2), y-(size/2), size, size);
	}
	
}
