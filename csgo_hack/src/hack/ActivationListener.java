package hack;

import org.jnativehook.keyboard.NativeKeyEvent;
import org.jnativehook.keyboard.NativeKeyListener;

public class ActivationListener implements NativeKeyListener {

	private int activationKeyCode = Integer.MIN_VALUE;
	private int deactivationKeyCode = Integer.MIN_VALUE;
	
	
	public ActivationListener(int activationKeyCode, int deactivationKeyCode) {
		setActivationKeyCode(activationKeyCode);
		setDeactivationKeyCode(deactivationKeyCode);
	}

	public int getActivationKeyCode() {
		return activationKeyCode;
	}

	public void setActivationKeyCode(int activationKeyCode) {
		this.activationKeyCode = activationKeyCode;
	}

	public int getDeactivationKeyCode() {
		return deactivationKeyCode;
	}

	public void setDeactivationKeyCode(int deactivationKeyCode) {
		this.deactivationKeyCode = deactivationKeyCode;
	}

	@Override
	public void nativeKeyPressed(NativeKeyEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void nativeKeyReleased(NativeKeyEvent arg0) {
		if(arg0.getKeyCode() == activationKeyCode && !HackControl.isHackActive()){
			HackControl.startHack();
		}else if(arg0.getKeyCode() == deactivationKeyCode && HackControl.isHackActive()){
			HackControl.stopHack();
		}
	}

	@Override
	public void nativeKeyTyped(NativeKeyEvent arg0) {
		// TODO Auto-generated method stub

	}

}
